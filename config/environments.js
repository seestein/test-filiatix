
export default {
  development: (config) => ({
    compiler_public_path: `http://${config.server_host}:${config.server_port}/`
    //при необходимости можно заюзать прокси
    /*proxy: {
      enabled: false,
      options: {
        host: 'http://localhost:8000',
        match: /^\/api\/.*!/
      }
    }*/
  }),
  production: (config) => ({
    compiler_public_path: '/',
    compiler_fail_on_warning: false,
    compiler_hash_type: 'chunkhash',
    compiler_devtool: null,
    compiler_stats: {
      chunks: true,
      chunkModules: false,
      colors: true
    }
  })
}
