
import deepFreeze from 'deep-freeze';
import movies from 'components/home/home.reducer';

const genres = ['Action', 'Adventure', 'Animation', 'Drama', 'Fantasy', 'Horror', 'Thriller'];

const initialState = {
  moviesList: [],
  genres
};

describe('Home reducers', () => {
  it('Should add movie', () => {
    let movie = {
      title: 'Title',
      genre: 2,
      year: 1989,
      rating: 3
    };

    expect(movies(initialState, deepFreeze({
      type: 'ADD_MOVIE',
      movie
    }))).to.deep.equal(deepFreeze({
      moviesList: [movie],
      genres
    }));
  });

  it('Should return default state', () => {
    expect(movies(initialState, deepFreeze({
      type: 'unknown',
    }))).to.deep.equal(deepFreeze(initialState));
  });
});
