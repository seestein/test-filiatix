
import deepFreeze from 'deep-freeze';
import { addMovie } from 'components/home/home.actions';

describe('Home actions', () => {
  it('Should return addMovie action', () => {
    let movie = {
      title: 'Title',
      genre: 2,
      year: 1989,
      rating: 3
    };

    expect(addMovie(deepFreeze(movie))).to.deep.equal({
      type: 'ADD_MOVIE',
      movie
    });
  });
});
