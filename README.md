### Запуск ###

* `npm install`

* `npm run flow` - проверка кода, типов переменных с помощью flow

* `npm run lint` - проверка синтаксиса кода с помощью eslint

* `npm run test` - запуск тестов, создание отчета по покрытию кода тестами (<PROJECT>/coverage)

* `npm run test:watch` - запуск тестов, ход выполнения можно наблюдать здесь http://localhost:9876/

* `npm run start:dev` - запуск приложения в режиме разработки, доступно по адресу http://localhost:3030/

* `npm run deploy` - сборка проекта для production, сохраняется в <PROJECT>/dist

* `npm run start:prod` - запуск приложения в режиме production, доступно по адресу http://localhost:3030/