// @flow

import React from 'react';
import AppBar from 'material-ui/AppBar';
import { GridList, GridTile } from 'material-ui/GridList';
import './core-layout.component.scss';

type Props = {
  children: HTMLElement
};

export const CoreLayout = (props: Props) => (
  <div>
    <AppBar title="КиноПоиск" iconElementLeft={<div />} style={{ marginBottom: 20 }} />

    <GridList cols={1} padding={1} cellHeight="auto">
      <GridTile cols={1}>
        {props.children}
      </GridTile>
    </GridList>
  </div>
);

CoreLayout.propTypes = {
  children: React.PropTypes.node.isRequired
};

export default CoreLayout;
