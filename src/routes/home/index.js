// @flow

import HomeContainer from 'components/home/home.container';

// Sync route definition
export default {
  component: HomeContainer
};
