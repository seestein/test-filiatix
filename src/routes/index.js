// @flow

import CoreLayout from 'layouts/core-layout/core-layout.component';
import Home from 'routes/home';

export const createRoutes = () => ({
  path: '/',
  component: CoreLayout,
  indexRoute: Home
});

export default createRoutes;
