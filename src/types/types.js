// @flow

export type Movie = {
  title: string,
  genre: number,
  year: number
};

export type Genre = string;

export type Action = { type: 'ADD_MOVIE', movie: Movie };

export type MoviesState = {
  moviesList: Array<Movie>,
  genres: Array<Genre>
};

export type State = {
  movies: MoviesState
};
