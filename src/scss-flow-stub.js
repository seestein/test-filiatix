// @flow

// we override flow to use this module whenever
// it uses scss-modules imported by webpack
// we simply say it is an object
// with string keys and string values

type CSSModule = { [key: string]: string };

const emptyCSSModule: CSSModule = {};

export default emptyCSSModule;
