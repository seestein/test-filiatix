import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import movies from 'components/home/home.reducer';

export const makeRootReducer = asyncReducers => combineReducers({
  // Add sync reducers here
  router,
  ...asyncReducers,
  form: formReducer,
  movies
});

export const injectReducer = (store: Object, { key, reducer }: Object) => {
  store.asyncReducers[key] = reducer; // eslint-disable-line
  store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
