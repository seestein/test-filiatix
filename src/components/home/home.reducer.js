// @flow

import type { MoviesState, Action } from 'types/types';
import initialMovies from '../../movies.json';

const genres = ['Action', 'Adventure', 'Animation', 'Drama', 'Fantasy', 'Horror', 'Thriller'];

const initialState = {
  moviesList: initialMovies,
  genres
};

export default function movies(state: MoviesState = initialState, action: Action): MoviesState {
  const moviesList = Array.from(state.moviesList);
  switch (action.type) {
    case 'ADD_MOVIE':
      moviesList.push(action.movie);
      return {
        ...state,
        moviesList
      };
    default:
      return state;
  }
}
