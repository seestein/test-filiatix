// @flow

import React from 'react';
import SaveMovieDialog from 'components/save-movie-dialog/save-movie-dialog.component';
import Rating from 'components/rating/rating.component';

import TableView from 'components/table/table.component';
import type { Genre, Movie } from 'types/types';

type Props = {
  movies: Array<Movie>,
  genres: Array<Genre>,
  addMovie: () => void
}

const Home = (props: Props) => {
  const movies = props.movies.map(movie => ({ ...movie, genre: props.genres[movie.genre] }));

  return (
    <TableView
      data={movies}
      columns={[
        { name: 'title', title: 'Movie title' },
        { name: 'genre', title: 'Genre' },
        { name: 'year', title: 'Year' },
        { name: 'rating', title: 'Rating', render: item => <Rating initialRate={item.rating} readonly /> }
      ]}
      searchBy="title"
      sortBy="title"
      dir="asc"
      addButtonTitle="Add movie"
      onSave={props.addMovie}
      saveDialog={SaveMovieDialog}
    />
  );
};

Home.propTypes = {
  movies: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
  genres: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
  addMovie: React.PropTypes.func.isRequired
};

export default Home;
