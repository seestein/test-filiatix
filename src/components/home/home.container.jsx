// @flow

import { connect } from 'react-redux';
import type { State } from 'types/types';
import { addMovie } from './home.actions';
import Home from './home.component';

const mapStateToProps = (state: State) => ({
  movies: state.movies.moviesList,
  genres: state.movies.genres
});

export default connect(mapStateToProps, {
  addMovie
})(Home);
