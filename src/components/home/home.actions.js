// @flow

import type { Movie, Action } from 'types/types';

export function addMovie(movie: Movie): Action {
  return {
    type: 'ADD_MOVIE',
    movie
  };
}
