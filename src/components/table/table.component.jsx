// @flow

import React, { Component } from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import DropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
import DropUpIcon from 'material-ui/svg-icons/navigation/arrow-drop-up';
import sorty from 'sorty';
import './table.component.scss';

type Props = {
  onSave: () => void,
  saveDialog: Object,
  data: Array<Object>,
  columns: Array<Object>,
  searchBy: string,
  sortBy: string,
  dir: string,
  addButtonTitle: string
}

export default class TableView extends Component {
  handleChangeSearch: (e: Event) => void;
  handleToggleSaveDialog: () => void;
  state: {
    isSaveDialogVisible: boolean,
    sortBy: string,
    dir: string,
    search: string
  };
  props: Props;

  constructor(props: Props) {
    super(props);

    this.state = {
      isSaveDialogVisible: false,
      sortBy: props.sortBy || '',
      dir: props.dir || 'asc',
      search: ''
    };

    this.handleChangeSearch = this.handleChangeSearch.bind(this);
    this.handleToggleSaveDialog = this.handleToggleSaveDialog.bind(this);
  }

  handleChangeSearch(e: Event, value: string) {
    this.setState({ search: value });
  }

  handleToggleSaveDialog() {
    this.setState({ isSaveDialogVisible: !this.state.isSaveDialogVisible });
  }

  handleSortBy(columnName: string) {
    let dir = this.state.dir === 'asc' ? 'desc' : 'asc';

    if (this.state.sortBy !== columnName) {
      dir = 'asc';
    }

    this.setState({ sortBy: columnName, dir });
  }

  renderTableHeader() {
    const { saveDialog: SaveDialog } = this.props;

    return (
      <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
        {SaveDialog &&
          <TableRow>
            <TableHeaderColumn colSpan={this.props.columns.length - 1}>
              <RaisedButton label={this.props.addButtonTitle || 'Add item'} onClick={this.handleToggleSaveDialog} />
            </TableHeaderColumn>
            <TableHeaderColumn>
              <TextField
                hintText="Search"
                fullWidth
                onChange={this.handleChangeSearch}
              />
            </TableHeaderColumn>
          </TableRow>
        }
        <TableRow>
          {this.props.columns.map(column =>
            <TableHeaderColumn key={column.name}>
              <div className="table-header" onClick={() => this.handleSortBy(column.name)}>
                {column.title}
                {this.state.sortBy === column.name && ((this.state.dir === 'asc' && <DropUpIcon />) || <DropDownIcon />)}
              </div>
            </TableHeaderColumn>
          )}
        </TableRow>
      </TableHeader>
    );
  }

  renderTableBody() {
    let items = this.props.data;
    sorty([{ name: this.state.sortBy, dir: this.state.dir }], items);

    items = items.filter(item => new RegExp(this.state.search, 'i').test(item[this.props.searchBy]));

    return (
      <TableBody displayRowCheckbox={false}>
        {(items.length &&
          items.map((item, i) => (
            <TableRow key={i}>
              {this.props.columns.map(column =>
                <TableRowColumn key={column.name}>
                  {column.render ? column.render(item) : item[column.name]}
                </TableRowColumn>
              )}
            </TableRow>
          ))
          ) ||
          <TableRow>
            <TableRowColumn colSpan={this.props.columns.length}>
              No results
            </TableRowColumn>
          </TableRow>
        }
      </TableBody>
    );
  }

  render() {
    const { saveDialog: SaveDialog } = this.props;

    return (
      <div>
        <Table selectable={false}>
          {this.renderTableHeader()}
          {this.renderTableBody()}
        </Table>

        {this.state.isSaveDialogVisible &&
          <SaveDialog
            onSave={this.props.onSave}
            onClose={this.handleToggleSaveDialog}
          />
        }
      </div>
    );
  }
}

TableView.propTypes = {
  onSave: React.PropTypes.func.isRequired,
  saveDialog: React.PropTypes.any.isRequired,
  data: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
  columns: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
  searchBy: React.PropTypes.string.isRequired,
  sortBy: React.PropTypes.string.isRequired,
  dir: React.PropTypes.string.isRequired,
  addButtonTitle: React.PropTypes.string.isRequired
};
