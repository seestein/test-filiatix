// @flow

import { connect } from 'react-redux';
import type { State } from 'types/types';
import SaveMovieDialogForm from './save-movie-dialog-form.component';

const mapStateToProps = (state: State) => ({
  genres: state.movies.genres
});

export default connect(mapStateToProps)(SaveMovieDialogForm);
