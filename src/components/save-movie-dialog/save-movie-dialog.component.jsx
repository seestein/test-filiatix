// @flow

import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import { Checkbox } from 'redux-form-material-ui';

import type { Movie } from 'types/types';
import SaveMovieDialogFormContainer from './save-movie-dialog-form.container';
import './save-movie-dialog.component.scss';

type Props = {
  change: () => void,
  reset: () => void,
  handleSubmit: () => void,
  onSave: (d: Movie) => void,
  onClose: () => void,
  invalid: boolean,
  submitting: boolean,
  pristine: boolean
}

class SaveMovieDialog extends Component {
  props: Props;
  handleSaveMovie: () => void;

  constructor(props: Props) {
    super(props);

    this.handleSaveMovie = this.handleSaveMovie.bind(this);
  }

  handleSaveMovie(data: Object) {
    this.props.onSave({
      title: data.movieTitle,
      genre: data.movieGenre,
      year: data.movieYear,
      rating: data.movieRating
    });

    this.props.reset();
    this.props.change('isCreateNextActive', data.isCreateNextActive);

    if (!data.isCreateNextActive) {
      this.props.onClose();
    }
  }

  render() {
    const { handleSubmit, pristine, submitting, invalid } = this.props;
    const actions = [
      <Field
        className="create-next"
        name="isCreateNextActive"
        component={Checkbox}
        onCheck={value => this.props.change('isCreateNextActive', value)}
        label="Create next movie"
      />,
      <FlatButton
        label="Cancel"
        onTouchTap={this.props.onClose}
      />,
      <FlatButton
        label="Save"
        primary
        onTouchTap={handleSubmit(this.handleSaveMovie)}
        disabled={pristine || submitting || invalid}
      />
    ];

    return (
      <Dialog
        title="New movie"
        actions={actions}
        modal={false}
        open
        autoScrollBodyContent
        onRequestClose={this.props.onClose}
      >
        <SaveMovieDialogFormContainer />
      </Dialog>
    );
  }
}

SaveMovieDialog.propTypes = {
  change: React.PropTypes.func.isRequired,
  reset: React.PropTypes.func.isRequired,
  handleSubmit: React.PropTypes.func.isRequired,
  onSave: React.PropTypes.func.isRequired,
  onClose: React.PropTypes.func.isRequired,
  invalid: React.PropTypes.bool,
  submitting: React.PropTypes.bool,
  pristine: React.PropTypes.bool
};

export default reduxForm({
  form: 'SaveMovieDialogForm'
})(SaveMovieDialog);
