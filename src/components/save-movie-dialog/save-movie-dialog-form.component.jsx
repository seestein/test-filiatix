// @flow

import React from 'react';
import { Field, reduxForm } from 'redux-form';
import MenuItem from 'material-ui/MenuItem';
import { GridList, GridTile } from 'material-ui/GridList';
import { SelectField, TextField } from 'redux-form-material-ui';
import Rating from 'components/rating/rating.component';
import './save-movie-dialog-form.component.scss';

const required = value => (!value && value !== 0 ? 'Required' : undefined);

const SaveMovieDialogForm = props => (
  <div>
    <Field
      name="movieTitle"
      component={TextField}
      hintText="Movie title"
      floatingLabelText="Movie title"
      fullWidth
      validate={required}
    />

    <GridList cols={2} padding={4} cellHeight="auto">
      <GridTile cols={1}>
        <Field
          name="movieGenre"
          component={SelectField}
          hintText="Genre"
          floatingLabelText="Genre"
          fullWidth
          maxHeight={200}
          validate={required}
        >
          {props.genres.map((genre, i) => <MenuItem key={i} value={i} primaryText={genre} />)}
        </Field>
      </GridTile>
      <GridTile cols={1}>
        <Field
          name="movieYear"
          component={SelectField}
          hintText="Year"
          floatingLabelText="Year"
          fullWidth
          maxHeight={200}
          validate={required}
        >
          {Array.from(Array(50).keys())
            .map(v =>
              <MenuItem
                key={v + 1}
                value={new Date().getFullYear() - v}
                primaryText={new Date().getFullYear() - v}
              />
            )
          }
        </Field>
      </GridTile>
    </GridList>

    <div className="rating">
      <Field
        name="movieRating"
        component={({ input }) => (
          <Rating initialRate={parseInt(input.value, 10)} onChange={v => props.change('movieRating', v)} />
        )}
        validate={required}
      />
    </div>

  </div>
);

SaveMovieDialogForm.propTypes = {
  genres: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
  change: React.PropTypes.func.isRequired
};

export default reduxForm({
  form: 'SaveMovieDialogForm'
})(SaveMovieDialogForm);
