// @flow

import React from 'react';
import ReactRating from 'react-rating';
import StarIcon from 'material-ui/svg-icons/toggle/star';

const Rating = (props: Object) => (
  <ReactRating
    empty={<StarIcon />}
    full={<StarIcon style={{ color: 'orange' }} />}
    {...props}
  />
);

export default Rating;
